import json
import requests
from models import db, Item, Result
from peewee import JOIN
from config import SCRIPT_PATH, POOL_SIZE
from subprocess import Popen, PIPE
from multiprocessing import Pool


# Polling API url for data
def get_data(target_url):
    api_url = 'http://web.archive.org/cdx/search/cdx?url={}'.format(target_url)
    print('Getting API response from {}...'.format(api_url))
    response = requests.get(api_url).text
    lines = response.split('\n')

    response = []
    for line in lines:
        try:
            urlkey, timestamp, original_url, mimetype, status_code, digest, length = line.split()
            item_dict = dict(urlkey=urlkey,
                             target_url=target_url,
                             timestamp=int(timestamp),
                             original_url=original_url,
                             mimetype=mimetype,
                             status_code=int(status_code),
                             digest=digest,
                             length=int(length)
                             )
            response.append(item_dict)
        except ValueError:
            pass
    print('Done. {} items discovered...'.format(len(lines)))
    return response


# Polling API url for data and Writing it to db
def data_to_db(target_url):
    item_dicts = get_data(target_url)
    print('Inserting downloaded items to database...')
    with db.atomic():
        for i in range(0, len(item_dicts), 5000):
            print('Inserting items chunk # {}'.format(i))
            Item.insert_many(item_dicts[i:i + 5000]).execute()
    print('Insertion done...')


# Running external script and geting results as json
def run_script(item):
    print('Processing item.id : {}'.format(item.id))
    with Popen([SCRIPT_PATH, item.original_url], stdout=PIPE) as proc:
        result = str(proc.stdout.read())
        result = result.split("'")[1][:-2]
        json_results = json.loads(result)
    return json_results


# Json array to results database
@db.execution_context(with_transaction=False)
def write_results(json_results, item):
    if json_results:
        for result in json_results:
            with db.atomic():
                Result.insert(name=result['name'],
                              cat=result['cat'],
                              snapshot=item.id).execute()


# Atomic "item --> results process
def item_to_results(item):
    write_results(run_script(item), item)


# Bulk external script running. Mapping item_to_results to pool of processes
def items_to_results(items):
    pool = Pool(POOL_SIZE)
    pool.map(item_to_results, generator(items))


# generator wrapper for items
def generator(items):
    for item in items:
        yield item


# all together
def process_url(target_url):
    data_to_db(target_url)
    unprocessed_items = Item.select().where(Item.target_url == target_url).execute()
    items_to_results(unprocessed_items)


# TO_DO : Resume unprocessed items.
def resume_processing():
    unprocessed_items = Item.select().join(Result, join_type=JOIN.LEFT_OUTER).where(Result.snapshot == None).execute()
    items_to_results(unprocessed_items)

