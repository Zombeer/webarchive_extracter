## Webarchive fetching + external multiprocessing script running tool.

### Features:

- Fetching webarchive api
- Resuming unprocessed items

### Requrements:
python3 interpreter + ```pip install -r requirements.txt```

### Before using:
All settings stored in _config.py_ file.
Please, set up correct path to external script and correct database credentials.

### Usage:

To process new query:

```python runner.py [target_url]```

Where _[target_url]_ is webarchive query. 
For example:
    
 ```python runner.py cnn.com```


To process unprocessed database items run:

```python runner.py -r```



