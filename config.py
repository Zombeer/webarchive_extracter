# Configuration file

# Database settings
DB_NAME = 'postgres'
DB_USER = 'postgres'
DB_PASSWORD = 'mysecretpassword'
DB_HOST = 'localhost'
DB_PORT = 5432

# External script settings
SCRIPT_PATH = '/Users/mac/Projects/webarchive_extracter/analyze.py'

# Pool params. Concurrent process count.
POOL_SIZE = 8
