from peewee import *
from config import DB_HOST, DB_PASSWORD, DB_PORT, DB_USER, DB_NAME

db = PostgresqlDatabase(DB_NAME, user=DB_USER, host=DB_HOST, port=DB_PORT, password=DB_PASSWORD)


class Item(Model):
    urlkey = CharField()
    timestamp = BigIntegerField()
    original_url = CharField()
    mimetype = CharField()
    status_code = IntegerField()
    digest = CharField()
    length = IntegerField()
    target_url = CharField()

    class Meta:
        database = db
        db_table = 'items'
        indexes = [(("target_url",), False)]


class Result(Model):
    snapshot = ForeignKeyField(Item)
    name = CharField()
    cat = IntegerField()

    class Meta:
        database = db
        db_table = 'results'
        # indexes = [(("snapshot",), False)]

db.create_tables([Item, Result], safe=True)
