"""Usage:
  runner.py <target_url>
  runner.py -r | --resume
  runner.py -h | --help | --version

All settings stored in config.py file.
"""
from docopt import docopt
from helpers import process_url, resume_processing

if __name__ == '__main__':
    arguments = docopt(__doc__, version='0.1.1rc')
    if arguments['<target_url>']:
        target_url = arguments['<target_url>']
        print('Starting script...\nTarget url is "{}"'.format(target_url))
        process_url(target_url)
    elif arguments['-r'] or arguments['--resume']:
        print('Resuming unprocessed items...')
        resume_processing()
